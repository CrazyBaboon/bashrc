# .bashrc
export HISTSIZE=10000
export HISTFILESIZE=50000
export HISTTIMEFORMAT="%h %d %H:%M "
export HISTIGNORE="ls:ll:pwd:clear:history:cd"

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

